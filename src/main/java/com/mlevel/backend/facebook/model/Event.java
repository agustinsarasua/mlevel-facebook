package com.mlevel.backend.facebook.model;

import com.google.api.server.spi.config.AnnotationBoolean;
import com.google.api.server.spi.config.ApiResourceProperty;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.*;
import com.mlevel.backend.facebook.integration.api.*;
import com.mlevel.backend.facebook.model.UserLike;

import java.util.Date;

@Entity(name="fb_event")
public class Event {

	@Id
	public String id;

	@ApiResourceProperty(ignored = AnnotationBoolean.TRUE)
	@Load
	public Ref<UserLike> userLikeId;

	@Index
	public Date startTime;

	public Date endTime;

	public Place place;

	public String name;

	public String description;

	public int attendingCount;

	public int maybeCount;

	public int interestedCount;

	public int declinedCount;

	public boolean canGuestsInvite;

	public boolean isCanceled;

	public String type;

	public String timezone;

	public boolean guestListEnabled;

	public int noreplyCount;

	public Date updatedTime;

	public com.mlevel.backend.facebook.integration.api.Cover cover;

	@Index
	public Date lastIndex;
}
