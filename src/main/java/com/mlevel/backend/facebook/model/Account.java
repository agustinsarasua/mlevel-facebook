package com.mlevel.backend.facebook.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import java.util.Date;
import java.util.List;

@Entity(name = "fb_account")
public class Account {

	@Id
	public String id;

	public String about;

	public String accessToken;

	public String category;

	public int fanCount;

	public boolean isPublished;

	public String link;

	public Location location;

	public String name;

	public int talkingAboutCount;

	public String website;

	public int checkins;

	public Cover cover;

	public List<Category> categoryList;

	public Date timestamp;

	@Index
	public Date lastIndexTime;

	public Account(){}

	public Account(String id, String name, String about, String category, double lat, double lon){
		this.name = name;
		this.about = about;
		this.category = category;
		this.id = id;
		this.location = new Location(lat, lon);
	}
}
