package com.mlevel.backend.facebook.model;

public class Category {

	public String id;

	public String name;

	public Category(){}

	public Category(String id, String name){
		this.id = id;
		this.name = name;
	}
}
