package com.mlevel.backend.facebook.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import java.util.Date;

@Entity
public class UserEntity {

    @Id
    private String id;

    @Index
    private String uniqueTag;

    private Date creationDate;

    private Location location;

    private String registrationToken;

    public UserEntity(){}

    public UserEntity(String id){
        this.id = id;
    }

    public UserEntity(String id, String uniqueTag){
        this.id = id;
        this.uniqueTag = uniqueTag;
    }

    public String getRegistrationToken() {
        return registrationToken;
    }

    public void setRegistrationToken(String registrationToken) {
        this.registrationToken = registrationToken;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getUniqueTag() {
        return uniqueTag;
    }

    public void setUniqueTag(String uniqueTag) {
        this.uniqueTag = uniqueTag;
    }
}
