package com.mlevel.backend.facebook.model;

public class Cover {

	private String id;

	private String source;

	private double offsetX;

	private double offsetY;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public double getOffsetX() {
		return offsetX;
	}

	public void setOffsetX(double offsetX) {
		this.offsetX = offsetX;
	}

	public double getOffsetY() {
		return offsetY;
	}

	public void setOffsetY(double offsetY) {
		this.offsetY = offsetY;
	}

	public Cover(){}

	public Cover(String id, String source){
		this.id = id;
		this.source = source;
	}

	public Cover(Cover cover){
		this.id = cover.id;
		this.source = cover.source;
		this.offsetX = cover.offsetX;
		this.offsetY = cover.offsetY;
	}
}
