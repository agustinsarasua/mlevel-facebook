package com.mlevel.backend.facebook.model;

public class Location {

    private double latitude;

    private double longitude;

    private String city;

    private String country;

    private String address;

    private String region;

    public Location() {
    }

    public Location(String address, double latitude, double longitude, String city, String country) {
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.city = city;
        this.country = country;
    }

    public Location(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Location(com.mlevel.backend.facebook.integration.api.Location location) {
        this.latitude = location.latitude;
        this.longitude = location.longitude;
        this.city = location.city;
        this.country = location.country;
        this.address = location.address;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
