package com.mlevel.backend.facebook.config;

import com.google.api.server.spi.guice.GuiceSystemServiceServletModule;
import com.googlecode.objectify.ObjectifyFilter;
import com.mlevel.backend.facebook.endpoint.EventEndpoint;
import com.mlevel.backend.facebook.endpoint.IndexEndpoint;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by agustin on 12/05/16.
 */
public class AppGuiceSystemServiceServletModule extends GuiceSystemServiceServletModule {

	@Override
	protected void configureServlets() {
		super.configureServlets();
		filter("/*").through(ObjectifyFilter.class);

		Set<Class<?>> serviceClasses = new HashSet<>();
		serviceClasses.add(IndexEndpoint.class);
		serviceClasses.add(EventEndpoint.class);
		this.serveGuiceSystemServiceServlet("/_ah/spi/*", serviceClasses);
	}

}
