package com.mlevel.backend.facebook.config;

import com.google.appengine.api.utils.SystemProperty;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RuntimeEnvironmentConfigurerListener implements ServletContextListener {

	public static final String KEY = "runtime.environment";
	public static final String DEVELOPMENT = "development";
	public static final String PRODUCTION = "production";

	public static final Properties properties = new Properties();

	private static final Logger LOGGER = Logger.getLogger(RuntimeEnvironmentConfigurerListener.class.getName());

	@Override
	public void contextInitialized(ServletContextEvent event) {
		try {
			if (System.getProperty(KEY) == null) {
				if (SystemProperty.environment.value() == SystemProperty.Environment.Value.Production) {
					InputStream app_properties_stream = event.getServletContext().getResourceAsStream("/WEB-INF/config/web_prod.properties");
					properties.load(app_properties_stream);
					System.setProperty(KEY, PRODUCTION);
				} else {
					LOGGER.info("Reading properties file");
					InputStream app_properties_stream = event.getServletContext().getResourceAsStream("/WEB-INF/config/web_dev.properties");
					LOGGER.info("Loading properties file");
					properties.load(app_properties_stream);
					LOGGER.info("Setting property env");
					System.setProperty(KEY, DEVELOPMENT);


				}
			}
			for(Object key : properties.keySet()){
				System.setProperty(key.toString(), properties.get(key).toString());
			}
			LOGGER.log(Level.INFO, System.getProperty(KEY));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
	}

}
