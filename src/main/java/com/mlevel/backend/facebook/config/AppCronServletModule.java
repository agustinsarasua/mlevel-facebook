package com.mlevel.backend.facebook.config;

import com.google.inject.servlet.ServletModule;
import com.mlevel.backend.facebook.servlet.FirebaseServlet;
import com.mlevel.backend.facebook.servlet.UserLikeWorker;

/**
 * Created by agustin on 15/05/16.
 */
public class AppCronServletModule extends ServletModule {

	@Override
	protected void configureServlets() {
//		serve("/worker/process-account-events").with(UserLikeWorker.class);
//		serve("/cron/indexUsers").with(IndexUserCron.class);
//		serve("/cron/indexEvents").with(IndexEventsCron.class);
		serve("/worker/process-user-like-events").with(UserLikeWorker.class);
		serve("/firebase").with(FirebaseServlet.class);
	}
}
