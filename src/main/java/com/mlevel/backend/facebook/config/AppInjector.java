package com.mlevel.backend.facebook.config;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.googlecode.objectify.ObjectifyFilter;

/**
 * Created by agustin on 12/05/16.
 */
public class AppInjector extends AbstractModule {

	@Override
	protected void configure() {

		bind(ObjectifyFilter.class).in(Singleton.class);
	}

}
