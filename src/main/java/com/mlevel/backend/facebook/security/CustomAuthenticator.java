package com.mlevel.backend.facebook.security;

import com.google.api.server.spi.auth.common.User;
import com.google.api.server.spi.config.Authenticator;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseToken;
import com.google.firebase.internal.NonNull;
import com.google.firebase.tasks.OnCompleteListener;
import com.google.firebase.tasks.OnSuccessListener;
import com.google.firebase.tasks.Task;
import com.google.inject.Singleton;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.atomic.AtomicBoolean;

@Singleton
public class CustomAuthenticator implements Authenticator {

	protected static final String HEADER_TEST = "X-PimbaTest";
	protected static final String HEADER_AUTHORIZATION = "X-PimbaAuth";
	public static final String TEST_USER_ID = "testUserId";

	@Override
	public User authenticate(HttpServletRequest httpRequest) {
		String authToken = httpRequest.getHeader(HEADER_AUTHORIZATION);
		//String testHeader = httpRequest.getHeader(HEADER_TEST);
		if(authToken == null){ return null; }
		if(!authToken.equalsIgnoreCase("TestAuthToken")){
			final AtomicBoolean done = new AtomicBoolean(false);
			Task<FirebaseToken> token = FirebaseAuth.getInstance().verifyIdToken(authToken)
					.addOnSuccessListener(new OnSuccessListener<FirebaseToken>() {
						@Override
						public void onSuccess(FirebaseToken decodedToken) {
							done.set(true);
						}

					})
					.addOnCompleteListener(new OnCompleteListener<FirebaseToken>() {
						@Override
						public void onComplete(@NonNull Task<FirebaseToken> task) {
							if(task.isSuccessful()){
								done.set(true);
							} else {
								done.set(true);
							}
						}
					});
			while (!done.get());
			return new User(token.getResult().getUid(), token.getResult().getEmail());
		} else {
			return new User(TEST_USER_ID, "test@pimba.com");
		}
	}

}
