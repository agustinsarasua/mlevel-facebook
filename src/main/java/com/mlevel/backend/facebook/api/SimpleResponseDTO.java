package com.mlevel.backend.facebook.api;

import java.io.Serializable;

public class SimpleResponseDTO implements Serializable {

	public String result;

	public String id;

	public SimpleResponseDTO(String result){
		this.result = result;
	}

	public SimpleResponseDTO(String result, String id){
		this.result = result;
		this.id = id;
	}

	public enum CommonResult {
		OK,
		ERROR
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
