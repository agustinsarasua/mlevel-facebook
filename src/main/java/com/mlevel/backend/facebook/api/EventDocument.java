package com.mlevel.backend.facebook.api;

import com.google.appengine.api.search.GeoPoint;
import com.googlecode.objectify.Key;
import com.mlevel.backend.facebook.integration.api.Cover;
import com.mlevel.backend.facebook.integration.api.Place;
import com.mlevel.backend.facebook.model.UserLike;

import java.util.Date;

/**
 * Created by agustin on 3/10/17.
 */
public class EventDocument {

    public String id;

    public Date startTime;

    public Date endTime;

    public GeoPoint location;

    public String name;

    public String description;

    public int attendingCount;

    public int maybeCount;

    public int interestedCount;

    public int declinedCount;

    public boolean canGuestsInvite;

    public boolean isCanceled;

    //No tiene sentido, siempre son "public"
//    public String type;

    public String timezone;

    public boolean guestListEnabled;

    public int noreplyCount;

    public Date updatedTime;

    public String src;

    public Date lastIndex;
}
