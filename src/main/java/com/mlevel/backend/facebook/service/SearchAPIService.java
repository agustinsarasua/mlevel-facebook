package com.mlevel.backend.facebook.service;

import com.google.api.client.util.Preconditions;
import com.google.api.server.spi.config.Singleton;
import com.google.api.server.spi.response.BadRequestException;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.InternalServerErrorException;
import com.google.appengine.api.search.*;
import com.mlevel.backend.facebook.api.EventDocument;
import com.mlevel.backend.facebook.model.Event;
import com.mlevel.backend.facebook.model.Location;
import com.mlevel.backend.facebook.util.SearchAPIUtil;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by agustin on 3/9/17.
 */
@Singleton
public class SearchAPIService {

    private static final Logger LOGGER = Logger.getLogger(SearchAPIService.class.getName());

    public static final int SORT_AND_SCORE_LIMIT = 1000;

    public enum IndexType {
        FB_EVENT
    }

    public static String getIndexName(IndexType type, DateTime dateTime) throws InternalServerErrorException {
        switch (type) {
            case FB_EVENT:
                return "fb_event_index_" + dateTime.getMonthOfYear() +"_"+ dateTime.getYear();
            default:
                throw new InternalServerErrorException("Not indexable type");
        }
    }

    public void indexUserEvent(Event event) {

        Document.Builder builder = Document.newBuilder()
                .setId(event.id)
                .addField(Field.newBuilder().setName("name").setText(event.name))
                .addField(Field.newBuilder().setName("description").setText(event.description))
                .addField(Field.newBuilder().setName("src").setText(event.cover.source))
                .addField(Field.newBuilder().setName("startTime").setDate(event.startTime))
                .addField(Field.newBuilder().setName("attendingCount").setNumber(event.attendingCount))
                .addField(Field.newBuilder().setName("interestedCount").setNumber(event.interestedCount))
                .addField(Field.newBuilder().setName("maybeCount").setNumber(event.maybeCount));

        if(event.endTime != null){
            builder =  builder.addField(Field.newBuilder().setName("endTime").setDate(event.endTime));
        }

        if(event.place != null && event.place.location != null){
            builder = builder.addField(Field.newBuilder().setName("location").setGeoPoint(new GeoPoint(event.place.location.latitude, event.place.location.longitude)));
        } else {
            builder = builder.addField(Field.newBuilder().setName("location").setGeoPoint(new GeoPoint(event.place.location.latitude, event.place.location.longitude)));
        }
        Document document = builder.build();
        try {
            String indexName = getIndexName(IndexType.FB_EVENT, new DateTime(event.startTime));
            LOGGER.log(Level.FINEST, "INDEX NAME: " + indexName);
            LOGGER.log(Level.FINEST, "Indexing document: " + document.toString());
            SearchAPIUtil.indexADocument(indexName, document);
        } catch (InterruptedException e) {
            LOGGER.log(Level.SEVERE, "Error while indexing events",e);
            return;
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, "Error while indexing events",e);
            return;
        }
    }

    public CollectionResponse<EventDocument> searchRecommendedEvents(String userId, int limit, String lastCursor, double distanceInMeters, Date fromDate, Location location)throws BadRequestException, InternalServerErrorException {
        try{
            Preconditions.checkNotNull(userId, "User id can not be null");
            Preconditions.checkState(distanceInMeters > 0, "Distance should be > 0");
        } catch (Exception e){
            throw new BadRequestException(e.getMessage());
        }

        DateTime fromDateTime = new DateTime(fromDate);

        List<Event> eventRefs = new ArrayList<>();
        List<EventDocument> searchResultDocuments = new ArrayList<>();
        long totalMatch;
        try {
            String indexName = getIndexName(IndexType.FB_EVENT, fromDateTime);

            Cursor cursor;
            if(lastCursor != null){
                cursor = Cursor.newBuilder().build(lastCursor);
            } else {
                cursor = Cursor.newBuilder().build();
            }
            // Build the SortOptions with 2 sort keys
            SortOptions sortOptions = SortOptions.newBuilder()
                    .addSortExpression(
                            SortExpression.newBuilder()
                                    .setExpression("attendingCount")
                                    .setDirection(SortExpression.SortDirection.DESCENDING))
                    .addSortExpression(
                            SortExpression.newBuilder()
                                    .setExpression("interestedCount")
                                    .setDirection(SortExpression.SortDirection.DESCENDING))
                    .addSortExpression(
                            SortExpression.newBuilder()
                                    .setExpression("startTime")
                                    .setDirection(SortExpression.SortDirection.DESCENDING))
                    .setLimit(SORT_AND_SCORE_LIMIT)
                    .build();

            // Build the QueryOptions
            QueryOptions options = QueryOptions.newBuilder()
                    .setLimit(limit)
                    .setCursor(cursor)
//                    .addExpressionToReturn(FieldExpression.newBuilder().setName("distance").setExpression("distance(location, geopoint("+ userEntity.getLocation().getLatitude() + "," + userEntity.getLocation().getLongitude() +"))"))
                    .setSortOptions(sortOptions)
                    .build();

            StringBuilder queryString = new StringBuilder();

//            int age = Years.yearsBetween(new DateTime(profile.getBirthday()), new DateTime()).getYears();

            queryString.append("userId = ").append(userId)
//                    .append(" AND maxAge >= ").append(age)
                    .append(" AND startTime >= ").append(new DateTime(fromDate).toString("yyyy-MM-dd"));
//                    .append(" AND (sex:").append(profile.getSex()).append(" OR sex:B)")
//                    .append(" AND distance(location, geopoint(").append(userEntity.getLocation().getLatitude()).append(",").append(userEntity.getLocation().getLongitude()).append(")) < ").append(distanceInMeters);

            //  Build the Query and run the search
            LOGGER.log(Level.INFO, queryString.toString());
            LOGGER.log(Level.FINEST, "INDEX NAME: " + indexName);
            Query query = Query.newBuilder().setOptions(options).build(queryString.toString());
            IndexSpec indexSpec = IndexSpec.newBuilder().setName(indexName).build();
            Index index = SearchServiceFactory.getSearchService().getIndex(indexSpec);
            if(index != null) {
                Results<ScoredDocument> result = index.search(query);
                int numberRetrieved = result.getNumberReturned();
                cursor = result.getCursor();
                totalMatch = result.getNumberFound();
                String webSafeCursor = cursor != null ? cursor.toWebSafeString() : null;

                if (numberRetrieved > 0) {
                    for (ScoredDocument doc : result.getResults()) {
                        EventDocument document = new EventDocument();
                        document.id = doc.getId();
                        document.name = doc.getOnlyField("name").getText();
                        document.description = doc.getOnlyField("description").getText();
                        document.startTime = doc.getOnlyField("startTime").getDate();
                        document.endTime = doc.getOnlyField("endTime").getDate();
                        document.location = doc.getOnlyField("location").getGeoPoint();
                        document.src = doc.getOnlyField("src").getText();
//                        document.distance = doc.getExpressions().get(0).getNumber();
                        searchResultDocuments.add(document);
                    }
                }
                return CollectionResponse.<EventDocument> builder().setItems(searchResultDocuments)
                        .setNextPageToken(webSafeCursor).build();

            } else {
                LOGGER.log(Level.WARNING, "index does not exists :" + indexName);
                return CollectionResponse.<EventDocument> builder().setItems(searchResultDocuments)
                        .setNextPageToken(null).build();
            }
        } catch (SearchException e) {
            // handle exception...
            e.printStackTrace();
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    public CollectionResponse<EventDocument> searchUserEvents(String userId, int limit, String lastCursor, double distanceInMeters, Date fromDate) throws BadRequestException, InternalServerErrorException {
        try{
            Preconditions.checkNotNull(userId, "User id can not be null");
            Preconditions.checkState(distanceInMeters > 0, "Distance should be > 0");
        } catch (Exception e){
            throw new BadRequestException(e.getMessage());
        }

        DateTime fromDateTime = new DateTime(fromDate);

        List<Event> eventRefs = new ArrayList<>();
        List<EventDocument> searchResultDocuments = new ArrayList<>();
        long totalMatch;
        try {
            String indexName = getIndexName(IndexType.FB_EVENT, fromDateTime);

            Cursor cursor;
            if(lastCursor != null){
                cursor = Cursor.newBuilder().build(lastCursor);
            } else {
                cursor = Cursor.newBuilder().build();
            }
            // Build the SortOptions with 2 sort keys
            SortOptions sortOptions = SortOptions.newBuilder()
                    .addSortExpression(
                            SortExpression.newBuilder()
                                    .setExpression("attendingCount")
                                    .setDirection(SortExpression.SortDirection.DESCENDING))
                    .addSortExpression(
                            SortExpression.newBuilder()
                                    .setExpression("interestedCount")
                                    .setDirection(SortExpression.SortDirection.DESCENDING))
                    .addSortExpression(
                            SortExpression.newBuilder()
                                    .setExpression("startTime")
                                    .setDirection(SortExpression.SortDirection.DESCENDING))
                    .setLimit(SORT_AND_SCORE_LIMIT)
                    .build();

            // Build the QueryOptions
            QueryOptions options = QueryOptions.newBuilder()
                    .setLimit(limit)
                    .setCursor(cursor)
//                    .addExpressionToReturn(FieldExpression.newBuilder().setName("distance").setExpression("distance(location, geopoint("+ userEntity.getLocation().getLatitude() + "," + userEntity.getLocation().getLongitude() +"))"))
                    .setSortOptions(sortOptions)
                    .build();

            StringBuilder queryString = new StringBuilder();

//            int age = Years.yearsBetween(new DateTime(profile.getBirthday()), new DateTime()).getYears();

            queryString.append("userId = ").append(userId)
//                    .append(" AND maxAge >= ").append(age)
                    .append(" AND startTime >= ").append(new DateTime(fromDate).toString("yyyy-MM-dd"));
//                    .append(" AND (sex:").append(profile.getSex()).append(" OR sex:B)")
//                    .append(" AND distance(location, geopoint(").append(userEntity.getLocation().getLatitude()).append(",").append(userEntity.getLocation().getLongitude()).append(")) < ").append(distanceInMeters);

            //  Build the Query and run the search
            LOGGER.log(Level.INFO, queryString.toString());
            LOGGER.log(Level.FINEST, "INDEX NAME: " + indexName);
            Query query = Query.newBuilder().setOptions(options).build(queryString.toString());
            IndexSpec indexSpec = IndexSpec.newBuilder().setName(indexName).build();
            Index index = SearchServiceFactory.getSearchService().getIndex(indexSpec);
            if(index != null) {
                Results<ScoredDocument> result = index.search(query);
                int numberRetrieved = result.getNumberReturned();
                cursor = result.getCursor();
                totalMatch = result.getNumberFound();
                String webSafeCursor = cursor != null ? cursor.toWebSafeString() : null;

                if (numberRetrieved > 0) {
                    for (ScoredDocument doc : result.getResults()) {
                        EventDocument document = new EventDocument();
                        document.id = doc.getId();
                        document.name = doc.getOnlyField("name").getText();
                        document.description = doc.getOnlyField("description").getText();
                        document.startTime = doc.getOnlyField("startTime").getDate();
                        document.endTime = doc.getOnlyField("endTime").getDate();
                        document.location = doc.getOnlyField("location").getGeoPoint();
                        document.src = doc.getOnlyField("src").getText();
//                        document.distance = doc.getExpressions().get(0).getNumber();
                        searchResultDocuments.add(document);
                    }
                }
                return CollectionResponse.<EventDocument> builder().setItems(searchResultDocuments)
                        .setNextPageToken(webSafeCursor).build();

            } else {
                LOGGER.log(Level.WARNING, "index does not exists :" + indexName);
                return CollectionResponse.<EventDocument> builder().setItems(searchResultDocuments)
                        .setNextPageToken(null).build();
            }
        } catch (SearchException e) {
            // handle exception...
            e.printStackTrace();
            throw new InternalServerErrorException(e.getMessage());
        }
    }
}
