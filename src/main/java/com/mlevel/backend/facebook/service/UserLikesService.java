package com.mlevel.backend.facebook.service;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.googlecode.objectify.Key;
import com.mlevel.backend.facebook.Constants;
import com.mlevel.backend.facebook.integration.FacebookRestClient;
import com.mlevel.backend.facebook.integration.api.Profile;
import com.mlevel.backend.facebook.model.UserEntity;
import com.mlevel.backend.facebook.model.UserLike;
import com.mlevel.backend.facebook.util.MemCacheUtil;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.mlevel.backend.facebook.service.OfyService.ofy;

@Singleton
public class UserLikesService {

	private static final Logger LOGGER = Logger.getLogger(UserLikesService.class.getName());

	private static final String MEM_PREFIX = "user_like_";


	@Inject
	private FacebookRestClient fbRestClient;

	public void indexUserLikes(String fbUserId, String accessToken, String userId){

		//Profile profile = this.fbRestClient.getUserProfile(accessToken);

		List<UserLike> userLikes = this.fbRestClient.loadUserLikes(fbUserId, accessToken, 20);

		for(UserLike userLike : userLikes){

			if(!MemCacheUtil.wasRecentlyProcessed(MEM_PREFIX, userLike.getId(), Boolean.TRUE.toString(), MemCacheUtil.USER_LIKE_EXPIRATION)){
				UserLike savedUserLike = ofy().load().type(UserLike.class).id(userLike.getId()).now();
				if(savedUserLike == null || new DateTime(savedUserLike.getLastUpdate()).plusDays(30).isBefore(DateTime.now())){
					userLike.setLastUpdate(DateTime.now().toDate());
					Key<UserLike> userLikeKey = ofy().save().entity(userLike).now();
//					UserEntity userEntity = ofy().load().type(UserEntity.class).id(userId).now();
					LOGGER.log(Level.INFO, "Registering UserLike for index "+ userLike.getId());
					Queue accountQueue = QueueFactory.getQueue(Constants.USERLIKE_PAGE_QUEUE);
					accountQueue.add(TaskOptions.Builder.withUrl("/worker/process-user-like-events").param("userLikeId", userLike.getId()).param("access_token", accessToken).param("userId", userId));
				}
			}

		}

	}


}
