package com.mlevel.backend.facebook.service;

import com.google.inject.Singleton;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;
import com.mlevel.backend.facebook.model.Event;
import com.mlevel.backend.facebook.model.UserEntity;
import com.mlevel.backend.facebook.model.UserLike;

@Singleton
public class OfyService {

	static {
		ObjectifyService.factory().register(UserLike.class);
		ObjectifyService.factory().register(UserEntity.class);
		ObjectifyService.factory().register(Event.class);
//		ObjectifyService.factory().register(OpenInvitation.class);
//		ObjectifyService.factory().register(Profile.class);
//		ObjectifyService.factory().register(UserEntity.class);
//		ObjectifyService.factory().register(UserPage.class);
//		ObjectifyService.factory().register(Invitation.class);
//
//		//Facebook Entities
//		ObjectifyService.factory().register(Account.class);
	}

	public static Objectify ofy() {
		return ObjectifyService.ofy();
	}

	public static ObjectifyFactory factory() {
		return ObjectifyService.factory();
	}
}
