package com.mlevel.backend.facebook.service;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.mlevel.backend.facebook.integration.FacebookRestClient;
import com.mlevel.backend.facebook.integration.api.DebugToken;
import com.mlevel.backend.facebook.model.Event;
import com.mlevel.backend.facebook.integration.api.Profile;
import com.mlevel.backend.facebook.model.Account;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.mlevel.backend.facebook.service.OfyService.ofy;


@Singleton
public class FacebookService {

	private static final Logger LOGGER = Logger.getLogger(FacebookService.class.getName());

	@Inject
	private FacebookRestClient restClient;

	public DebugToken debugAccessToken(String accessToken){
		DebugToken debugToken = restClient.debugUserAccessToken(accessToken);
		return debugToken;
	}

	public Profile getUserPublicProfile(String userId){
		Profile profile = restClient.getUserPublicProfile(userId);
		return profile;
	}

//	public void indexAccountEvents(String accountId, String accessToken, String userId){
//		Queue accountQueue = QueueFactory.getQueue(Constants.ACCOUNT_PAGE_QUEUE);
//		accountQueue.add(TaskOptions.Builder.withUrl("/worker/process-account-events").param("accountId", accountId).param("access_token", accessToken).param("userId", userId));
//	}

//	public Account saveAccountAndIndex(Account account, String userId, String accessToken) throws BadRequestException {
//		try {
//			Preconditions.checkNotNull(account, "Account can not be null");
//			Preconditions.checkNotNull(account.id, "Account id can not be null");
//			Preconditions.checkNotNull(userId, "UserId can not be null");
//			Preconditions.checkNotNull(accessToken, "AccessToken can not be null");
//		} catch (Exception e){
//			throw new BadRequestException(e.getMessage());
//		}
//		LOGGER.log(Level.INFO, "Registering Facebook Page "+ account.id);
//		Queue accountQueue = QueueFactory.getQueue(Constants.ACCOUNT_PAGE_QUEUE);
//
//		LOGGER.log(Level.INFO, "Saving new Account instance");
//		account.timestamp = DateTime.now().toDate();
//		Key<Account> key = ofy().save().entity(account).now();
//		accountQueue.add(TaskOptions.Builder.withUrl("/worker/process-account-events").param("accountId", account.id).param("access_token", accessToken).param("userId", userId));
//		LOGGER.log(Level.INFO, "Key = " + key.getId());
//		return account;
//	}

	public Account loadPage(String id) {
		final Account fbPage = ofy().load().type(Account.class).id(id).now();
		return fbPage;
	}

	public List<Account> loadUserPages(String id) {
		LOGGER.log(Level.INFO, "Loading user accounts...");
		return ofy().load().type(Account.class).filter("userId", id).list();
	}

	public List<Event> loadAccountEvents(String accountId, String accessToken){
		return restClient.loadAccountEvents(accountId, accessToken);
	}

}
