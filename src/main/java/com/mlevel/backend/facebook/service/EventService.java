package com.mlevel.backend.facebook.service;

import com.google.api.server.spi.response.BadRequestException;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.InternalServerErrorException;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.mlevel.backend.facebook.api.EventDocument;
import com.mlevel.backend.facebook.model.Event;
import com.mlevel.backend.facebook.model.Location;
import com.mlevel.backend.facebook.model.UserEntity;
import org.joda.time.DateTime;

import java.util.*;

import static com.mlevel.backend.facebook.service.OfyService.ofy;

@Singleton
public class EventService {

	@Inject
	private SearchAPIService searchAPIService;

	public Collection<Event> loadUserEvents(String userId, String cursorStr) throws InternalServerErrorException, BadRequestException {

		UserEntity userEntity = ofy().load().type(UserEntity.class).id(userId).now();
		List<Event> userEvents = ofy().load().type(Event.class).filter("startTime >", DateTime.now().toDate()).list();
//		String[] eventIds = new String[userEvents.size()];
//		for(int i = 0; i< userEvents.size(); i++){
//			eventIds[i] = userEvents.get(i).id;
//		}

//		Collection<Event> events = ofy().load().type(Event.class).ids(Arrays.asList(eventIds)).values();
//		return events;
		return userEvents;
		//return searchAPIService.searchUserEvents(userId, 30, cursorStr, 2000, DateTime.now().toDate());
	}

	public CollectionResponse<EventDocument> loadRecommendedEvents(String userId, Location location) throws InternalServerErrorException, BadRequestException {

		UserEntity userEntity = ofy().load().type(UserEntity.class).id(userId).now();

		return searchAPIService.searchRecommendedEvents(userId, 4, null, 2000, DateTime.now().toDate(), location);
	}
}
