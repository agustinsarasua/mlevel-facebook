package com.mlevel.backend.facebook.util;

import com.google.appengine.api.memcache.ErrorHandlers;
import com.google.appengine.api.memcache.Expiration;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;

import java.util.logging.Level;

public class MemCacheUtil {

	public static final int USER_LIKE_EXPIRATION = 60;
	public static final int TOKEN_EXPIRATION_CACHE_SECONDS = 60;

	public static boolean wasRecentlyProcessed(String prefix, String key, String nValue, int expirationInSeconds){
		MemcacheService syncCache = MemcacheServiceFactory.getMemcacheService();
		syncCache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));

		byte[] value = (byte[]) syncCache.get(prefix + key);
		if(value == null){
			syncCache.put(key, nValue, Expiration.byDeltaSeconds(expirationInSeconds));
			return false;
		}
		return true;
	}
}
