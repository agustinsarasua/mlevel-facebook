package com.mlevel.backend.facebook.util;


import com.google.appengine.api.search.*;
import org.joda.time.DateTime;

import java.util.Date;

/**
 * Created by agustin on 8/26/16.
 */
public class SearchAPIUtil {

    /**
     * Put a given document into an index with the given indexName.
     * @param indexName The name of the index.
     * @param document A document to add.
     * @throws InterruptedException When Thread.sleep is interrupted.
     */
    // [START putting_document_with_retry]
    public static void indexADocument(String indexName, Document document)
            throws InterruptedException {
        IndexSpec indexSpec = IndexSpec.newBuilder().setName(indexName).build();
        Index index = SearchServiceFactory.getSearchService().getIndex(indexSpec);
        final int maxRetry = 3;
        int attempts = 0;
        int delay = 2;
        while (true) {
            try {
                index.put(document);
            } catch (PutException e) {
                if (StatusCode.TRANSIENT_ERROR.equals(e.getOperationResult().getCode())
                        && ++attempts < maxRetry) { // retrying
                    Thread.sleep(delay * 1000);
                    delay *= 2; // easy exponential backoff
                    continue;
                } else {
                    throw e; // otherwise throw
                }
            }
            break;
        }
    }
    // [END putting_document_with_retry]

    /**
     * Para poder buscar for tiempo - las 00:00 -> 10000 , las 23:59 -> 12359
     * @param dateTime
     * @return
     */
    public static int parseToSearchableTime(DateTime dateTime){
        String hourOfDay = String.valueOf(dateTime.getHourOfDay());
        String minuteOfDay = String.valueOf(dateTime.getMinuteOfHour());
        if(hourOfDay.length() == 1){
            hourOfDay = "0" + hourOfDay;
        }

        if(minuteOfDay.length() == 1){
            minuteOfDay = "0" + minuteOfDay;
        }
        return Integer.valueOf("1" + hourOfDay + minuteOfDay);
    }

    public static DateTime parseFromSearchableTime(int time, Date date){
        String hour = String.valueOf(time).substring(1, 3);
        String minute = String.valueOf(time).substring(3, 5);

        DateTime dateTime = new DateTime(date).withTime(Integer.valueOf(hour).intValue(), Integer.valueOf(minute).intValue(), 0, 0);
        return dateTime;
    }
}
