package com.mlevel.backend.facebook;

/**
 * Created by agustin on 28/04/16.
 */
public class Constants {

	public static final String INDEX_EVENT_QUEUE = "index-event-queue";
	public static final String INDEX_USER_QUEUE = "index-user-queue";
	public static final String USERLIKE_PAGE_QUEUE = "user-like-process-queue";

	public static final String DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";

	public static final String APP_ACCESS_TOKEN = "1709613959262846|_XCZ7ieEvNPHE4sEJTOK659iFDY";

	// https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2
	public static final String HEADER_COUNTRY = "X-AppEngine-Country";
	public static final String HEADER_REGION = "X-AppEngine-Region";
	public static final String HEADER_CITY = "X-AppEngine-City";
	public static final String HEADER_CITY_LATLONG = "X-AppEngine-CityLatLong";

}
