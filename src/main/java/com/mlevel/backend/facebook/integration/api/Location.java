package com.mlevel.backend.facebook.integration.api;

/**
 * Created by agustin on 14/07/16.
 */
public class Location {

	public double latitude;

	public double longitude;

	public String city;

	public String country;

	public String address;
}
