package com.mlevel.backend.facebook.integration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.mlevel.backend.facebook.integration.api.DebugToken;
import com.mlevel.backend.facebook.model.Event;
import com.mlevel.backend.facebook.integration.api.Profile;
import com.mlevel.backend.facebook.integration.api.TestUser;
import com.mlevel.backend.facebook.integration.request.EntityRequest;
import com.mlevel.backend.facebook.integration.request.ListRequest;
import com.mlevel.backend.facebook.model.UserLike;

import javax.servlet.ServletException;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

@Singleton
public class FacebookRestClient {

	@Inject
	private HttpClient httpClient;

	public static final String HOST = "https://graph.facebook.com/v2.7/";

	public static final String CLIENT_ID = System.getProperty("facebook-app-id");

	public static final String CLIENT_SECRET = System.getProperty("facebook-app-secret");

	public static final String APP_ACCESS_TOKEN = CLIENT_ID + "|" + CLIENT_SECRET;

	public static final String DEBUG_TOKEN_URL = "debug_token?input_token={}&access_token={}";

	public static final String USER_PROFILE_URL = "{}?fields=last_name,birthday,cover,email,name,id&access_token={}";

	public static final String PAGE_EVENTS_URL = "{}/events?fields=id,start_time,end_time,place,name,description,attending_count,maybe_count,is_canceled,declined_count,cover,ticket_uri,can_guests_invite,type,category,interested_count,timezone,guest_list_enabled,is_page_owned,noreply_count,updated_time&limit={}&access_token={}";

	public static final String USER_LIKES_URL = "{}/likes?fields=id,name,about,placeType,cover,location,category,fan_count&limit={}&access_token={}";

	public static final String LONG_TERM_TOKEN = "oauth/access_token?grant_type=fb_exchange_token&client_id={}&client_secret={}&fb_exchange_token={}";

	/**
	 * Services for tests (testUsers)
	 */
	public static final String GET_TEST_USERS = System.getProperty("facebook-app-id") + "/accounts/test-users?access_token={}";


	//FIXME terminar de arreglar el metodo
	public void exchangeAccessToken(String shortLivedAccessToken){
		try {
			EntityRequest<DebugToken> debugToken = httpClient.doGet(HOST + LONG_TERM_TOKEN, new TypeReference<EntityRequest<DebugToken>>(){},
					CLIENT_ID,
					CLIENT_SECRET,
					URLEncoder.encode(shortLivedAccessToken, "UTF-8"));
//			return debugToken.notification;
		} catch (IOException | ServletException e) {
			e.printStackTrace();
		}
//		return null;
	}

	public DebugToken debugUserAccessToken(String accessToken){
		try {
			EntityRequest<DebugToken> debugToken = httpClient.doGet(HOST + DEBUG_TOKEN_URL, new TypeReference<EntityRequest<DebugToken>>(){},
					URLEncoder.encode(accessToken, "UTF-8"),
					URLEncoder.encode(APP_ACCESS_TOKEN, "UTF-8"));
			return debugToken.data;
		} catch (IOException | ServletException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Profile getUserProfile(String accessToken){
		try {
			Profile profile = httpClient.doGet(HOST + USER_PROFILE_URL, new TypeReference<Profile>(){},
					"me",
					URLEncoder.encode(accessToken, "UTF-8"));
			return profile;
		} catch (IOException | ServletException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Profile getUserPublicProfile(String userId){
		try {
			Profile profile = httpClient.doGet(HOST + USER_PROFILE_URL, new TypeReference<Profile>(){},
					userId,
					URLEncoder.encode(APP_ACCESS_TOKEN, "UTF-8"));
			return profile;
		} catch (IOException | ServletException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<Event> loadAccountEvents(String accountId, String accessToken){
		try {
			ListRequest<Event> eventList = httpClient.doGet(HOST + PAGE_EVENTS_URL, new TypeReference<ListRequest<Event>>(){},
				accountId,
				"10",
				URLEncoder.encode(accessToken, "UTF-8"));
			if(eventList != null) {
				return eventList.data;
			}
		} catch (IOException | ServletException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<TestUser> loadTestUsers(){
		try {
			ListRequest<TestUser> eventList = httpClient.doGet(HOST + GET_TEST_USERS, new TypeReference<ListRequest<TestUser>>(){},
					URLEncoder.encode(APP_ACCESS_TOKEN, "UTF-8"));
			if(eventList != null) {
				return eventList.data;
			}
		} catch (IOException | ServletException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<UserLike> loadUserLikes(String userId, String accessToken, int limit){
		try {
			ListRequest<UserLike> eventList = httpClient.doGet(HOST + USER_LIKES_URL, new TypeReference<ListRequest<UserLike>>(){},
				userId, String.valueOf(limit), URLEncoder.encode(accessToken, "UTF-8"));
			if(eventList != null) {
				return eventList.data;
			}
		} catch (IOException | ServletException e) {
			e.printStackTrace();
		}
		return null;
	}
}
