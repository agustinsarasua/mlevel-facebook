package com.mlevel.backend.facebook.integration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.google.inject.Singleton;

import javax.servlet.ServletException;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@Singleton
public class HttpClient {

	private static final Logger LOGGER = Logger.getLogger(HttpClient.class.getName());

	public <R> R doGet(String uri, TypeReference<R> typeRef, String... params) throws IOException, ServletException {

		HttpURLConnection conn = null;
		InputStream is = null;
		Object result = null;
		try {
			//constants
			URL url = new URL(this.buildUrl(uri, params));

			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

			conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(10000 /*milliseconds*/);
			conn.setConnectTimeout(15000 /* milliseconds */);
			conn.setRequestMethod("GET");
			conn.setDoInput(true);
			conn.setDoOutput(true);

			//make some HTTP header nicety
			conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
			conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");

			//open
			conn.connect();

			is = conn.getInputStream();

			int respCode = conn.getResponseCode();
			if (typeRef != null && (respCode == HttpURLConnection.HTTP_OK || respCode == HttpURLConnection.HTTP_NOT_FOUND)) {
				StringBuffer response = new StringBuffer();
				String line;

				BufferedReader reader = new BufferedReader(new InputStreamReader(is));
				while ((line = reader.readLine()) != null) {
					response.append(line);
				}
				reader.close();
				LOGGER.info("Response - " + response.toString());
				return objectMapper.readValue(response.toString(), typeRef);
			} else {
			}
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} finally {
			conn.disconnect();
		}
		return null;
	}

	public <R> R doDelete(String uri, Map<String, String> headers, String... params) throws IOException, ServletException{
		LOGGER.log(Level.INFO, "DELETE " + uri);
		return this.execute("DELETE", uri, "", null, headers, params);
	}

	public <R> R  doPost(String uri, Object body, TypeReference<R> typeRef, Map<String, String> headers, String... params)
		throws IOException, ServletException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		String message = objectMapper.writeValueAsString(body);

		LOGGER.log(Level.INFO, "Body - " + message);
		return this.execute("POST", uri, message, typeRef, headers, params);
	}

	public <R> R doPost(String uri, String body, TypeReference<R> typeRef, Map<String, String> headers, String... params)
		throws IOException, ServletException {

		LOGGER.log(Level.INFO, "Body - " + body);
		return this.execute("POST", uri, body, typeRef, headers, params);
	}

	public <R> R doPut(String uri, String body, TypeReference<R> typeRef, Map<String, String> headers, String... params)
		throws IOException, ServletException {

		LOGGER.log(Level.INFO, "Body - " + body);
		return this.execute("PUT", uri, body, typeRef, headers, params);
	}


	private <R> R  execute(String method, String uri, String body, TypeReference<R> typeRef, Map<String, String> headers, String... params)
		throws IOException, ServletException {
		// [START complex]
		String buildUrl = this.buildUrl(uri, params);
		URL url = new URL(buildUrl);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod(method);
		//make some HTTP header nicety
		conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
		conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");

		if(headers != null && !headers.isEmpty()){
			for(String key : headers.keySet()){
				conn.setRequestProperty(key, headers.get(key));
			}
		}

		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
		writer.write(body);
		writer.close();

		int respCode = conn.getResponseCode();
		LOGGER.log(Level.INFO, "Url: " + method + " " + buildUrl + " - RespCode: " + conn.getResponseCode());
		if (typeRef != null && (respCode == HttpURLConnection.HTTP_OK || respCode == HttpURLConnection.HTTP_NOT_FOUND)) {
			StringBuffer response = new StringBuffer();
			String line;

			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			while ((line = reader.readLine()) != null) {
				response.append(line);
			}
			reader.close();
			LOGGER.info("Response - " + response.toString());
			return objectMapper.readValue(response.toString(), typeRef);
		} else {
		}

		return null;
	}

	private String buildUrl(String serviceUrl, Object[] uriParams) {
		String urlSuffix = null;
		if(uriParams != null && uriParams.length > 0) {
			String partialUrl = serviceUrl.replaceAll("\\{\\}", "%s");
			urlSuffix = String.format(partialUrl, uriParams);
		} else {
			urlSuffix = serviceUrl;
		}
		return urlSuffix;
	}
}
