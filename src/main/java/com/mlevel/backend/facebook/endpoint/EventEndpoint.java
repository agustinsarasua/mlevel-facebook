package com.mlevel.backend.facebook.endpoint;

import com.google.api.server.spi.auth.common.User;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.response.BadRequestException;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.InternalServerErrorException;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.inject.Inject;
import com.mlevel.backend.facebook.Constants;
import com.mlevel.backend.facebook.api.EventDocument;
import com.mlevel.backend.facebook.model.Event;
import com.mlevel.backend.facebook.model.Location;
import com.mlevel.backend.facebook.security.CustomAuthenticator;
import com.mlevel.backend.facebook.service.EventService;

import javax.servlet.http.HttpServletRequest;
import java.util.logging.Level;
import java.util.logging.Logger;

@Api(name = "facebook",
	version = "v1",
	authenticators = {CustomAuthenticator.class})
public class EventEndpoint {

	private static final Logger LOGGER = Logger.getLogger(EventEndpoint.class.getName());

	@Inject
	private EventService eventService;

	@ApiMethod(name = "getUserEvents", httpMethod = ApiMethod.HttpMethod.GET, path = "events")
	public CollectionResponse<Event> loadUserEvents(@Named("cursor") String cursor, User user) throws UnauthorizedException, BadRequestException, InternalServerErrorException {
		if (user == null) {
			throw new UnauthorizedException("Access denied!");
		}
		return CollectionResponse.<Event> builder().setItems(eventService.loadUserEvents(user.getId(), cursor)).build();
	}

	@ApiMethod(name = "getRecommendedEvents", httpMethod = ApiMethod.HttpMethod.GET, path = "events/recommended")
	public CollectionResponse<EventDocument> loadRecommendedEvents(@Named("cursor") String cursor, User user, HttpServletRequest request) throws UnauthorizedException, BadRequestException, InternalServerErrorException {
		if (user == null) {
			throw new UnauthorizedException("Access denied!");
		}
		String latLong = request.getHeader(Constants.HEADER_CITY_LATLONG);
		if(latLong == null){
			LOGGER.log(Level.SEVERE, "No Header with Latitude Longitude Found!! Setting default..");
			latLong = "0,0";
		}
		String[] latLongs = latLong.split(",");
		Location location = new Location(Double.valueOf(latLongs[0]), Double.valueOf(latLongs[1]));

		return eventService.loadRecommendedEvents(user.getId(), location);
	}
}
