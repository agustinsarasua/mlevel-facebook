package com.mlevel.backend.facebook.endpoint;

import com.google.api.server.spi.auth.common.User;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.memcache.ErrorHandlers;
import com.google.appengine.api.memcache.Expiration;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.inject.Inject;
import com.mlevel.backend.facebook.Constants;
import com.mlevel.backend.facebook.api.SimpleResponseDTO;
import com.mlevel.backend.facebook.security.CustomAuthenticator;
import com.mlevel.backend.facebook.service.FacebookService;
import com.mlevel.backend.facebook.service.UserLikesService;
import com.mlevel.backend.facebook.util.MemCacheUtil;

import java.util.logging.Level;

@Api(name = "facebook",
	version = "v1",
	authenticators = {CustomAuthenticator.class})
public class IndexEndpoint {

	@Inject
	private UserLikesService userLikesService;

	@ApiMethod(name = "indexToken",
		path = "index",
		httpMethod = ApiMethod.HttpMethod.POST)
	public SimpleResponseDTO indexToken(@Named("fb_user_id") String fbUserId, @Named("access_token") String accessToken , User user) throws UnauthorizedException {
		if (user == null) {
			throw new UnauthorizedException("Access denied!");
		}

		// First check it is in the cache, it means that the token was already indexed
		MemcacheService syncCache = MemcacheServiceFactory.getMemcacheService();
		syncCache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));

		String key = user.getId();
		String value = (String) syncCache.get(key);
		if(value == null){
			syncCache.put(key, accessToken, Expiration.byDeltaSeconds(MemCacheUtil.TOKEN_EXPIRATION_CACHE_SECONDS));
			userLikesService.indexUserLikes(fbUserId, accessToken, user.getId());
		} else {
			// Significa que ya lo indexe
		}
		return new SimpleResponseDTO(SimpleResponseDTO.CommonResult.OK.name());
	}



}
