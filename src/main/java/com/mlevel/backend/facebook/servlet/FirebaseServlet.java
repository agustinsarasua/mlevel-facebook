package com.mlevel.backend.facebook.servlet;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.inject.Singleton;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import java.io.InputStream;
import java.util.logging.Logger;

/**
 * Created by agustin on 14/06/16.
 */
@Singleton
public class FirebaseServlet extends HttpServlet {

	private static final Logger LOGGER = Logger.getLogger(FirebaseServlet.class.getName());

	@Override
	public void init(ServletConfig config) throws ServletException {
		try{
			InputStream in = config.getServletContext().getResourceAsStream("/WEB-INF/pimba-backend-30fc8c276b96.json");
			FirebaseOptions options = new FirebaseOptions.Builder()
				.setServiceAccount(in)
				.setDatabaseUrl("https://pimba-backend.firebaseio.com/")
				.build();
			FirebaseApp.initializeApp(options);
			LOGGER.info("Authentication enabled");
		}
		catch(Throwable t) {
			t.printStackTrace();
			LOGGER.warning("AUTHENTICATION DISABLED. Only public resources will be available");
		}
		super.init(config);
	}
}
