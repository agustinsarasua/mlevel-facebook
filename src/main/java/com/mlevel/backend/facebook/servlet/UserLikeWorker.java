package com.mlevel.backend.facebook.servlet;

import com.google.api.server.spi.response.BadRequestException;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.mlevel.backend.facebook.integration.api.Place;
import com.mlevel.backend.facebook.model.Event;
import com.mlevel.backend.facebook.model.UserEntity;
import com.mlevel.backend.facebook.model.UserLike;
import com.mlevel.backend.facebook.service.FacebookService;
import com.mlevel.backend.facebook.service.SearchAPIService;
import org.joda.time.DateTime;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.mlevel.backend.facebook.service.OfyService.ofy;

/**
 * This class Process receives a Account, AccessToken and UserId
 * It index all the events of that Account
 */

@Singleton
public class UserLikeWorker extends HttpServlet {

	private static final Logger LOGGER = Logger.getLogger(UserLikeWorker.class.getName());

	@Inject
	private FacebookService facebookService;

	@Inject
	private SearchAPIService searchAPIService;

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws IOException {
		String userLikeId = req.getParameter("userLikeId");
		String accessToken = req.getParameter("access_token");
		String userId = req.getParameter("userId");
		LOGGER.log(Level.INFO, "Running Account Event Worker for account = " + userLikeId);
		UserLike userLike = ofy().load().key(Key.create(UserLike.class, userLikeId)).now();
		// Si nunca fue indexada o pasaron mas de 15 dias desde la ultima indexacion, volver a indexar
		if(userLike.getLastIndex() == null || userLike.getLastIndex().before(DateTime.now().minusDays(5).toDate())){
			List<Event> events = facebookService.loadAccountEvents(userLike.getId(),accessToken);
			for (Event event : events){
				if((event.place == null || event.place.location == null) && userLike.getLocation() != null){
					LOGGER.log(Level.INFO, "Using userLike location, (no location present) " + event.id);
					event.place = new Place();
					event.place.location = new com.mlevel.backend.facebook.integration.api.Location();
					event.place.location.latitude = userLike.getLocation().getLatitude();
					event.place.location.longitude = userLike.getLocation().getLongitude();

				}
				if(event.place != null){
					event.userLikeId = Ref.create(Key.create(UserLike.class, userLikeId));
					LOGGER.log(Level.INFO, "Event UserLike " + userLikeId);
					try {
						processFacebookEvent(event);
					} catch (BadRequestException e) {
						e.printStackTrace();
						LOGGER.log(Level.SEVERE, e.getMessage(), e);
					}
				} else{
					LOGGER.log(Level.INFO, "No location in the event nor place - skipping index " + event.id);
				}

			}
			userLike.setLastIndex(DateTime.now().toDate());
			ofy().save().entity(userLike);
		}
		LOGGER.log(Level.INFO, "Account Event Worker for account = " + userLikeId + " finished");
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
		doGet(req, resp);
	}

	private void processFacebookEvent(Event fbEvent) throws BadRequestException {
		Event event = ofy().load().key(Key.create(Event.class, fbEvent.id)).now();
		if(fbEvent.cover != null){
			if((event == null || event.lastIndex.before(DateTime.now().minusDays(5).toDate())) && !fbEvent.isCanceled && new DateTime(fbEvent.startTime).isAfter(DateTime.now())) {
				fbEvent.lastIndex = DateTime.now().toDate();
				LOGGER.log(Level.INFO, "Event UserLike ");
				ofy().save().entity(fbEvent).now();
				//searchAPIService.indexUserEvent(event);
				LOGGER.log(Level.INFO, "Facebook Event Indexed");
			} else {
				LOGGER.log(Level.INFO, "Facebook Event is canceled "+ fbEvent.id);
			}
		}else{
			LOGGER.log(Level.SEVERE, "Event has no cover "+ fbEvent.id);
		}
	}
}
